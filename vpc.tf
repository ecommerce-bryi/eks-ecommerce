data "aws_availability_zones" "available" {
}

module "vpc-prod" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> 3.0"

  name                         = var.app_name
  cidr                         = var.cidr_block
  azs                          = data.aws_availability_zones.available.names
  create_database_subnet_group = true
  private_subnets              = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets               = ["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]
  database_subnets             = ["10.0.7.0/24", "10.0.8.0/24"]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.app_name}" = "shared"
    "kubernetes.io/role/elb"                = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.app_name}" = "shared"
  }

  tags = {
    Example    = "${var.name}"
    GithubRepo = "terraform-aws-eks"
    GithubOrg  = "terraform-aws-modules"
  }
}