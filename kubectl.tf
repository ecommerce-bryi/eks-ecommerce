provider "kubectl" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
  load_config_file       = false
}

locals {
  db_service     = "rds-service"
  cpu_request    = 50
  cpu_limit      = 500
  memory_request = 150
  memory_limit   = 200
  tls_secret     = "bryidomain"
}

data "kubectl_path_documents" "initial" {
  pattern = "./start-manifests/*.yaml"
}

resource "kubectl_manifest" "initial" {
  count     = length(data.kubectl_path_documents.initial.documents)
  yaml_body = element(data.kubectl_path_documents.initial.documents, count.index)
  depends_on = [
    module.eks,
    module.rds
  ]
}

resource "null_resource" "exec_kubectl" {
  provisioner "local-exec" {
    command = <<EOT
      cat kubeconfig_${var.app_name} > ~/.kube/config
      kubectl create secret tls ${local.tls_secret} --cert=tls/fullchain.pem --key=tls/privkey.pem
      kubectl -n kube-system set env daemonset aws-node ENABLE_POD_ENI=true
      kubectl -n kube-system rollout status ds aws-node
    EOT
  }
  depends_on = [
    kubectl_manifest.initial,
    aws_iam_role_policy_attachment.cni-cluster-role-attachment
  ]
}

resource "kubectl_manifest" "db_internal_endpoint" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  labels:
    app: ${local.db_service}
  name: ${local.db_service}
spec:
  externalName: ${module.rds.db_instance_address}
  selector:
    app: ${local.db_service}
  type: ExternalName
status:
  loadBalancer: {}   
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "db_secret" {
  yaml_body = <<YAML
apiVersion: v1
kind: Secret
metadata:
  name: ${var.app_name}-db
type: kubernetes.io/basic-auth
stringData:
  user: "${module.rds.db_instance_username}"
  password: "${data.aws_ssm_parameter.my_rds_password.value}"
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "allow_rds_access" {
  yaml_body = <<YAML
apiVersion: vpcresources.k8s.aws/v1beta1
kind: SecurityGroupPolicy
metadata:
  name: ${var.app_name}-allow-rds-access
spec:
  podSelector:
    matchLabels:
      component: ${var.app_name}-api
      component: ${var.app_name}-migrations
      component: ${var.app_name}-collectstatic
  securityGroups:
    groupIds:
      - ${aws_security_group.pod-sg.id}
YAML
  depends_on = [
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "migrations_job" {
  yaml_body = <<YAML
apiVersion: batch/v1
kind: Job
metadata:
  name: ${var.app_name}-migrations
spec:
  backoffLimit: 0
  template:
    metadata:
      labels:
        component: ${var.app_name}-migrations
    spec:
      restartPolicy: Never
      containers:
        - name: ${var.app_name}-migrations
          image: ${var.api_image}
          command: ["./migrations.sh"]
          env:
          - name: SECRET_KEY
            value: "${var.secret_key}"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: STRIPE_SECRET_KEY
            value: "${var.stripe_key}"
          - name: S3ACCESSKEYID
            value: "${var.s3accesskey}"
          - name: S3ACCESSKEY
            value: "${var.s3secretkey}"
          - name: S3BUCKETNAME
            value: "${var.s3bucketname}"
          - name: S3REGION
            value: "${var.s3region}"
YAML
  depends_on = [
    kubectl_manifest.allow_rds_access,
    kubectl_manifest.db_secret,
    kubectl_manifest.db_internal_endpoint
  ]
}

resource "kubectl_manifest" "collectstatic_job" {
  yaml_body = <<YAML
apiVersion: batch/v1
kind: Job
metadata:
  name: ${var.app_name}-collectstatic
spec:
  backoffLimit: 0
  template:
    metadata:
      labels:
        component: ${var.app_name}-colelctstatic
    spec:
      restartPolicy: Never
      containers:
        - name: ${var.app_name}-collectstatic
          image: ${var.api_image}
          command: ["python3", "manage.py", "collectstatic", "--noinput"]
          env:
          - name: SECRET_KEY
            value: "${var.secret_key}"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: STRIPE_SECRET_KEY
            value: "${var.stripe_key}"
          - name: S3ACCESSKEYID
            value: "${var.s3accesskey}"
          - name: S3ACCESSKEY
            value: "${var.s3secretkey}"
          - name: S3BUCKETNAME
            value: "${var.s3bucketname}"
          - name: S3REGION
            value: "${var.s3region}"
YAML
  depends_on = [
    kubectl_manifest.allow_rds_access,
    kubectl_manifest.db_secret,
    kubectl_manifest.db_internal_endpoint
  ]
}

resource "kubectl_manifest" "api_deployment" {
  yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${var.app_name}-api
spec:
  replicas: 1
  selector:
    matchLabels:
      component: ${var.app_name}-api
  template:
    metadata:
      labels:
        component: ${var.app_name}-api
    spec:
      containers:
        - name: ${var.app_name}-api
          image: ${var.api_image}
          command: ["./entrypoint.sh"]
          resources:
            requests:
              memory: "${local.memory_request}Mi"
              cpu: "${local.cpu_request}m"
            limits:
              memory: "${local.memory_limit}Mi"
              cpu: "${local.cpu_limit}m"
          ports:
            - containerPort: ${var.api_port}
          livenessProbe:
            httpGet:
              path: /health_check
              port: ${var.api_port}
            initialDelaySeconds: 30
            failureThreshold: 1
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /health_check 
              port: ${var.api_port}
            initialDelaySeconds: 60
            periodSeconds: 10
          env:
          - name: SECRET_KEY
            value: "${var.secret_key}"
          - name: DEBUG
            value: "0"
          - name: DB_NAME
            value: "${module.rds.db_instance_name}"
          - name: DB_USER
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: user
          - name: DB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: ${var.app_name}-db
                key: password
          - name: DB_HOST
            value: "${local.db_service}"
          - name: DB_PORT
            value: "5432"
          - name: STRIPE_SECRET_KEY
            value: "${var.stripe_key}"
          - name: S3ACCESSKEYID
            value: "${var.s3accesskey}"
          - name: S3ACCESSKEY
            value: "${var.s3secretkey}"
          - name: S3BUCKETNAME
            value: "${var.s3bucketname}"
          - name: S3REGION
            value: "${var.s3region}"
YAML
  depends_on = [
    kubectl_manifest.migrations_job,
    kubectl_manifest.collectstatic_job,
    helm_release.cluster-autoscaler
  ]
}

resource "kubectl_manifest" "frontend_deployment" {
  yaml_body = <<YAML
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${var.app_name}-frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      component: ${var.app_name}-frontend
  template:
    metadata:
      labels:
        component: ${var.app_name}-frontend
    spec:
      containers:
        - name: ${var.app_name}-frontend
          image: ${var.frontend_image}
          resources:
            requests:
              memory: "${local.memory_request}Mi"
              cpu: "${local.cpu_request}m"
            limits:
              memory: "${local.memory_limit}Mi"
              cpu: "${local.cpu_limit}m"
          ports:
            - containerPort: 80
          livenessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 30
            failureThreshold: 1
            periodSeconds: 10
          readinessProbe:
            httpGet:
              path: /
              port: 80
            initialDelaySeconds: 60
            periodSeconds: 10
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "api_service" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  name: ${var.app_name}-api-service
spec:
  type: ClusterIP
  ports:
    - port: ${var.api_port}
      targetPort: ${var.api_port}
  selector:
    component: ${var.app_name}-api
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "frontend_service" {
  yaml_body = <<YAML
apiVersion: v1
kind: Service
metadata:
  name: ${var.app_name}-frontend-service
spec:
  type: ClusterIP
  ports:
    - port: 80
      targetPort: 80
  selector:
    component: ${var.app_name}-frontend
YAML
  depends_on = [
    kubectl_manifest.api_deployment
  ]
}

resource "kubectl_manifest" "ingress" {
  yaml_body = <<YAML
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ${var.app_name}-ingress-service
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  tls:
    - hosts:
      - ${var.api_domain}
      - ${var.domain}
      secretName: ${local.tls_secret}
  rules:
    - host: ${var.api_domain}
      http:
        paths:
          - path: /(.*)
            pathType: Prefix
            backend:
              service:
                name: ${var.app_name}-api-service
                port: 
                  number: ${var.api_port}

    - host: ${var.domain}
      http:
        paths:
          - path: /(.*)
            pathType: Prefix
            backend:
              service:
                name: ${var.app_name}-frontend-service
                port: 
                  number: 80
YAML
  depends_on = [
    kubectl_manifest.api_deployment,
    kubectl_manifest.api_service,
    kubectl_manifest.frontend_deployment,
    kubectl_manifest.frontend_service,
    kubectl_manifest.initial,
    null_resource.exec_kubectl
  ]
}

resource "kubectl_manifest" "hpa_api" {
  yaml_body = <<YAML
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${var.app_name}-hpa-api
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${var.app_name}-api
  minReplicas: 1
  maxReplicas: 10
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 90
  - type: Resource
    resource:
      name: memory
      target:
        type: AverageValue
        averageValue: 400Mi 
YAML
  depends_on = [
    kubectl_manifest.api_deployment,
    kubectl_manifest.initial,
  ]
}

resource "kubectl_manifest" "hpa_frontend" {
  yaml_body = <<YAML
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${var.app_name}-hpa-frontend
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${var.app_name}-frontend
  minReplicas: 1
  maxReplicas: 5
  metrics:
  - type: Resource
    resource:
      name: cpu
      target:
        type: Utilization
        averageUtilization: 90
  - type: Resource
    resource:
      name: memory
      target:
        type: AverageValue
        averageValue: 400Mi 
YAML
  depends_on = [
    kubectl_manifest.frontend_deployment,
    kubectl_manifest.initial
  ]
}