terraform init 
terraform fmt
terraform validate
terraform apply --auto-approve

cd route53*
terraform init 

LB_ARN=$(aws elbv2 describe-load-balancers \
--query 'LoadBalancers[0].LoadBalancerArn' | tr -d '"')

terraform import aws_lb.nlb $LB_ARN

terraform fmt
terraform validate

terraform apply --auto-approve