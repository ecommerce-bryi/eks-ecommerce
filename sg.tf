module "security_group_rds" {
  source                  = "./aws_security_group"
  port_list               = ["0"]
  resource_name           = "RDS-SG"
  vpc_id                  = module.vpc-prod.vpc_id
  allowed_security_groups = tolist([aws_security_group.pod-sg.id])
  depends_on = [
    aws_security_group.pod-sg
  ]
}

resource "aws_security_group" "pod-sg" {
  name   = "Dynamic Security Group - POD"
  vpc_id = module.vpc-prod.vpc_id

  ingress {
    protocol    = "tcp"
    from_port   = 0
    to_port     = 0
    cidr_blocks = [var.cidr_block]
    self        = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = tolist(["0.0.0.0/0"])
  }

  tags = {
    Name  = "Dynamic Security Group - POD-SG"
    Owner = "Alexey Bryi"
  }
}

module "security_group_nodes" {
  source                  = "./aws_security_group"
  port_list               = ["0"]
  allowed_security_groups = tolist([aws_security_group.pod-sg.id])
  resource_name           = "NODES-SG"
  vpc_id                  = module.vpc-prod.vpc_id
  depends_on = [
    aws_security_group.pod-sg
  ]
}