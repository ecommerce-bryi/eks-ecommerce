provider "aws" {
  region = var.region
}

terraform {
  backend "s3" {
    bucket = "eks-bryi"
    key    = "store/terraform.tfstate"
    region = "us-east-1"
  }
}

data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.24.0"

  cluster_version = "1.21"
  cluster_name    = var.app_name
  vpc_id          = module.vpc-prod.vpc_id
  enable_irsa     = true
  subnets         = module.vpc-prod.private_subnets

  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

  worker_additional_security_group_ids = [
    module.security_group_nodes.security_group_id
  ]

  worker_groups = [
    {
      name                 = "spot"
      spot_price           = "0.199"
      instance_type        = "m5.large"
      asg_max_size         = 5
      asg_desired_capacity = 2
      kubelet_extra_args   = "--node-labels=node.kubernetes.io/lifecycle=spot"
      suspended_processes  = ["AZRebalance"]
      tags = [
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "false"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${var.name}"
          "propagate_at_launch" = "false"
          "value"               = "owned"
        }
      ]
    }
  ]

  tags = {
    Example    = var.name
    GithubRepo = "terraform-aws-eks"
    GithubOrg  = "terraform-aws-modules"
  }

  depends_on = [
    module.vpc-prod
  ]
}