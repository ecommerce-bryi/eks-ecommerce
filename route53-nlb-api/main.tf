provider "aws" {
  region = var.region
}

locals {
  alias_ns  = "api.bryidomain.tk"
  domain_ns = "bryidomain.tk"
}

terraform {
  backend "s3" {
    bucket = "eks-bryi"
    key    = "store/route53-terraform.tfstate"
    region = "us-east-1"
  }
}


resource "aws_lb" "nlb" {
  load_balancer_type = "network"
}

resource "aws_route53_zone" "primary_zone" {
  name = local.domain_ns
}

resource "aws_route53_record" "api_alias" {
  zone_id = aws_route53_zone.primary_zone.zone_id
  name    = local.alias_ns
  type    = "A"

  alias {
    name                   = aws_lb.nlb.dns_name
    zone_id                = aws_lb.nlb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    aws_route53_zone.primary_zone
  ]
}

resource "aws_route53_record" "main_alias" {
  zone_id = aws_route53_zone.primary_zone.zone_id
  name    = local.domain_ns
  type    = "A"

  alias {
    name                   = aws_lb.nlb.dns_name
    zone_id                = aws_lb.nlb.zone_id
    evaluate_target_health = true
  }

  depends_on = [
    aws_route53_zone.primary_zone
  ]
}